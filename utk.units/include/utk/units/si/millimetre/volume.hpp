// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.units/utk.units/include/utk/units/si/millimetre/volume.hpp
//
// Description: Declaration of SI millimetre system volume units.


#ifndef UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_VOLUME_HPP
#define UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_VOLUME_HPP


#include <boost/units/physical_dimensions/volume.hpp>
#include <boost/units/static_constant.hpp>
#include <boost/units/unit.hpp>

#include "utk/units/si/millimetre/system.hpp"


namespace utk {
	namespace units {
		namespace si {
			namespace millimetre_system {
				inline namespace v1 {
					using volume = boost::units::unit<
					    boost::units::volume_dimension,
					    millimetre_system::system >;

					BOOST_UNITS_STATIC_CONSTANT (cubic_millimeter, volume);
					BOOST_UNITS_STATIC_CONSTANT (cubic_millimeters, volume);
					BOOST_UNITS_STATIC_CONSTANT (cubic_millimetre, volume);
					BOOST_UNITS_STATIC_CONSTANT (cubic_millimetres, volume);
				} // namespace v1
			}     // namespace millimetre_system


			using millimetre_system::cubic_millimeter;
			using millimetre_system::cubic_millimeters;
			using millimetre_system::cubic_millimetre;
			using millimetre_system::cubic_millimetres;
		} // namespace si
	}     // namespace units
} // namespace utk


#endif /* UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_VOLUME_HPP */
