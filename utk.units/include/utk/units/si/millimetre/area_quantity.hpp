// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.units/utk.units/include/utk/units/si/millimetre/area_quantity.hpp
//
// Description: Declaration of SI millimetre system area quantity types.


#ifndef UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_QUANTITY_HPP
#define UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_QUANTITY_HPP


#include <boost/units/quantity.hpp>

#include "utk/units/si/millimetre/area.hpp"


namespace utk {
	namespace units {
		namespace si {
			inline namespace v1 {
				using square_millimetre_d =
				    boost::units::quantity< millimetre_system::area, double >;

				using square_millimetre_f =
				    boost::units::quantity< millimetre_system::area, float >;

				using square_millimetre_i =
				    boost::units::quantity< millimetre_system::area, int >;
			} // namespace v1
		}     // namespace si
	}         // namespace units
} // namespace utk


#endif /* UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_QUANTITY_HPP */
