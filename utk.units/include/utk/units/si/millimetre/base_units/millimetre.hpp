// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.units/utk.units/include/utk/units/si/millimetre/base_units/millimetre.hpp
//
// Description: Declaration of SI millimetre base unit declaration.


#ifndef UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_BASE_UNITS_MILLIMETRE_HPP
#define UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_BASE_UNITS_MILLIMETRE_HPP


#include <boost/units/config.hpp>

#include <boost/units/base_unit.hpp>
#include <boost/units/base_units/si/meter.hpp>
#include <boost/units/scaled_base_unit.hpp>
#include <boost/units/static_rational.hpp>


namespace utk {
	namespace units {
		namespace si {
			inline namespace v1 {
				using millimetre_base_unit = boost::units::scaled_base_unit<
				    boost::units::si::meter_base_unit,
				    boost::units::
				        scale< 10, boost::units::static_rational< -3 > > >;
			} // namespace v1
		}     // namespace si
	}         // namespace units
} // namespace utk


#if BOOST_UNITS_HAS_BOOST_TYPEOF

#	include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP ()

BOOST_TYPEOF_REGISTER_TYPE (utk::units::si::millimetre_base_unit)

#endif


#endif // UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_BASE_UNITS_MILLIMETRE_HPP
