// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.units/utk.units/include/utk/units/si/millimetre/area.hpp
//
// Description: Declaration of SI millimetre system area units.


#ifndef UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_HPP
#define UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_HPP


#include <boost/units/physical_dimensions/area.hpp>
#include <boost/units/static_constant.hpp>
#include <boost/units/unit.hpp>

#include "utk/units/si/millimetre/system.hpp"


namespace utk {
	namespace units {
		namespace si {
			namespace millimetre_system {
				inline namespace v1 {
					using area = boost::units::unit<
					    boost::units::area_dimension,
					    millimetre_system::system >;

					BOOST_UNITS_STATIC_CONSTANT (square_millimetre, area);
					BOOST_UNITS_STATIC_CONSTANT (square_millimetres, area);
					BOOST_UNITS_STATIC_CONSTANT (square_millimeter, area);
					BOOST_UNITS_STATIC_CONSTANT (square_millimeters, area);
				} // namespace v1
			}     // namespace millimetre_system


			using millimetre_system::square_millimeter;
			using millimetre_system::square_millimeters;
			using millimetre_system::square_millimetre;
			using millimetre_system::square_millimetres;
		} // namespace si
	}     // namespace units
} // namespace utk


#endif /* UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_AREA_HPP */
