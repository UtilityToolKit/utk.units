// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.units/utk.units/include/utk/units/si/millimetre/system.hpp
//
// Description: Declaration of SI millimetre unit system declaration.


#ifndef UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_SYSTEM_HPP
#define UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_SYSTEM_HPP


#include <boost/units/dimensionless_type.hpp>
#include <boost/units/make_system.hpp>

#include "utk/units/si/millimetre/base_units/millimetre.hpp"


namespace utk {
	namespace units {
		namespace si {
			namespace millimetre_system {
				inline namespace v1 {
					using system =
					    boost::units::make_system< millimetre_base_unit >::type;

					using dimensionless = boost::units::
					    unit< boost::units::dimensionless_type, system >;
				} // namespace v1
			}     // namespace millimetre_system
		}         // namespace si
	}             // namespace units
} // namespace utk


#endif /* UTK_UNITS_INCLUDE_UTK_UNITS_SI_MILLIMETRE_SYSTEM_HPP */
